import { Component } from '@angular/core';
import { PokemonUser } from './components/molecules/interfaces/box-class.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'pokedex';

  pokemon: PokemonUser = new PokemonUser();
  constructor() { }

  ngOnInit(): void {
  }

  getPokemonSelected(pokemon: PokemonUser): void {
    this.pokemon = pokemon;
  }

}

