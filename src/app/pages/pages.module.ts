import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { ComponentsModule } from '../components/components.module';
import { PokemonDescriptionPageComponent } from './pokemon-description-page/pokemon-description-page.component';


@NgModule({
  declarations: [
    PokemonDescriptionPageComponent
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    PagesRoutingModule
  ], 
  exports: [
    PokemonDescriptionPageComponent
  ]
})
export class PagesModule { }
