import { Component, HostListener, Input, OnInit } from '@angular/core';
import { statusClassIcon } from 'src/app/components/atoms/interfaces/icon-interface';
import { PokemonUser } from 'src/app/components/molecules/interfaces/box-class.interface';

@Component({
  selector: 'app-pokemon-description-page',
  templateUrl: './pokemon-description-page.component.html',
  styleUrls: ['./pokemon-description-page.component.scss']
})
export class PokemonDescriptionPageComponent implements OnInit {

  @Input() pokemon: PokemonUser = new PokemonUser();
  @Input() iconHeight: string = '';
  @Input() iconWidth: string = '';
  icontStyle: statusClassIcon = statusClassIcon.BIG_IMG;

  constructor() { }

  ngOnInit(): void {
    console.log(this.pokemon)
    this.onResize();
  }

  @HostListener('window:resize', ['$event'])
  onResize(): void {
    const BREAK_POINT_TABLET: number = 768;
    const innerWidth = window.innerWidth;
    this.iconWidth = innerWidth < BREAK_POINT_TABLET ? '16rem' : '19rem'
}

}
