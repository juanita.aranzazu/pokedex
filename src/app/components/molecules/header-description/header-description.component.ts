import { Component, OnInit } from '@angular/core';
import { statusClassIcon } from '../../atoms/interfaces/icon-interface';
import { statusClassText } from '../../atoms/interfaces/text-class.interface';

@Component({
  selector: 'app-header-description',
  templateUrl: './header-description.component.html',
  styleUrls: ['./header-description.component.scss']
})
export class HeaderDescriptionComponent implements OnInit {

  textStyle: statusClassText = statusClassText.SMALL;
  icontStyle: statusClassIcon = statusClassIcon.SMALL_IMG;
  
  constructor() { }

  ngOnInit(): void {
  }

}
