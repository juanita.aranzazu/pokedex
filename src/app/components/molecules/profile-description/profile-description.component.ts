import { Component, Input, OnInit } from '@angular/core';
import { statusClassCard } from '../../atoms/interfaces/box-class.interface';
import { statusClassText } from '../../atoms/interfaces/text-class.interface';
import { PokemonUser } from '../interfaces/box-class.interface';

@Component({
  selector: 'app-profile-description',
  templateUrl: './profile-description.component.html',
  styleUrls: ['./profile-description.component.scss']
})
export class ProfileDescriptionComponent implements OnInit {

  @Input() pokemon: PokemonUser= new PokemonUser();
  cardStyle: statusClassCard = statusClassCard.GENERAL;
  textStyleSmallBl: statusClassText = statusClassText.SMALLEST;
  textStyleMedium: statusClassText = statusClassText.MEDIUM;

  constructor() { }

  ngOnInit(): void {
  }

}
