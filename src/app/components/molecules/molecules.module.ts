import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MoleculesRoutingModule } from './molecules-routing.module';
import { AtomsModule } from '../atoms/atoms.module';
import { IconButtonComponent } from './icon-button/icon-button.component';
import { ProfileDescriptionComponent } from './profile-description/profile-description.component';
import { TitleDescriptionComponent } from './title-description/title-description.component';
import { HeaderDescriptionComponent } from './header-description/header-description.component';


@NgModule({
  declarations: [
    IconButtonComponent,
    ProfileDescriptionComponent,
    TitleDescriptionComponent,
    HeaderDescriptionComponent
  ],
  imports: [
    CommonModule,
    MoleculesRoutingModule,
    AtomsModule,
  ],
  exports: [
    AtomsModule,
    IconButtonComponent,
    ProfileDescriptionComponent,
    TitleDescriptionComponent,
    HeaderDescriptionComponent
  ]
})
export class MoleculesModule { }
