import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { PokemonUser } from '../interfaces/box-class.interface';
import { firstValueFrom } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserPokemonService {

  constructor( private http: HttpClient) { }

  public async gePokemonsUsers(){
    return firstValueFrom(this.http.get<PokemonUser[]>('./assets/data/pokemons.json'))
    .then((value) => {
      return value;
    })
  }

}
