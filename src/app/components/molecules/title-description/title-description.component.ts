import { Component, Input, OnInit } from '@angular/core';
import { statusClassCard } from '../../atoms/interfaces/box-class.interface';
import { statusClassIcon } from '../../atoms/interfaces/icon-interface';
import { statusClassText } from '../../atoms/interfaces/text-class.interface';

@Component({
  selector: 'app-title-description',
  templateUrl: './title-description.component.html',
  styleUrls: ['./title-description.component.scss']
})
export class TitleDescriptionComponent implements OnInit {

  @Input() namePokemon: string = ''
  textStyleBig: statusClassText = statusClassText.BIG;
  cardStyle: statusClassCard = statusClassCard.GENERAL;
  icontStyle: statusClassIcon = statusClassIcon.SMALL_IMG;

  constructor() { }

  ngOnInit(): void {
  }

}
