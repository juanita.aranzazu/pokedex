import { Component, Input, OnInit } from '@angular/core';
import { statusClassButton } from '../../atoms/interfaces/button-class.interface';
import { statusClassIcon } from '../../atoms/interfaces/icon-interface';

@Component({
  selector: 'app-icon-button',
  templateUrl: './icon-button.component.html',
  styleUrls: ['./icon-button.component.scss']
})
export class IconButtonComponent implements OnInit {

  @Input() iconUrl: string = '';
  @Input() buttonHeight: string = '';
  @Input() buttonWidth: string = '';
  buttonStyle: statusClassButton = statusClassButton.FOOTER_YL;
  icontStyle: statusClassIcon = statusClassIcon.BUTTON_IMG;

  constructor() { }

  ngOnInit(): void {
  }

}
