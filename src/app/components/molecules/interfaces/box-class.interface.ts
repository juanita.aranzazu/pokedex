export class PokemonUser  {
  name: string = '';
  num: number = 0;
  level: number = 0;
  type: string = '';
  ability:  string = '';
  height:  string = '';
  weight:  string = '';
  image:  string = '';
}