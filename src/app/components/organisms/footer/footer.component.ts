import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { statusClassIcon } from '../../atoms/interfaces/icon-interface';
import { statusClassText } from '../../atoms/interfaces/text-class.interface';
import { PokemonUser } from '../../molecules/interfaces/box-class.interface';
import { UserPokemonService } from '../../molecules/services/user-pokemon.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  @Output() emitPokemon = new EventEmitter<PokemonUser>();
  textStyleMd: statusClassText = statusClassText.MEDIUM;
  icontStyle: statusClassIcon = statusClassIcon.BUTTON_IMG;
  pokemons: PokemonUser[] = [];

  constructor(private userPokemonService: UserPokemonService) { }

  async ngOnInit(): Promise<void> {
    this.pokemons = await this.userPokemonService.gePokemonsUsers();
    this.emitPokemon.emit(this.pokemons[0]);
  }

  sendPokemonSelected(pokemon: PokemonUser): void {
    this.emitPokemon.emit(pokemon);
  }
}
