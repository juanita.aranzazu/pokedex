import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrganismsRoutingModule } from './organisms-routing.module';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MoleculesModule } from '../molecules/molecules.module';


@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,
    OrganismsRoutingModule,
    MoleculesModule
  ],
  exports: [
    MoleculesModule,
    HeaderComponent,
    FooterComponent
  ]
})
export class OrganismsModule { }
