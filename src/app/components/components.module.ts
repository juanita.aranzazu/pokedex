import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsRoutingModule } from './components-routing.module';
import { TemplatesModule } from './templates/templates.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TemplatesModule,
    ComponentsRoutingModule
  ],
  exports: [TemplatesModule],
})
export class ComponentsModule { }
