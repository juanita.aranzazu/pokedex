export enum statusClassIcon {
  BIG_IMG = 'figure__icon__big',
  SMALL_IMG = 'figure__icon__small',
  BUTTON_IMG = 'figure__icon__button',
  DEFAULT = '',
}