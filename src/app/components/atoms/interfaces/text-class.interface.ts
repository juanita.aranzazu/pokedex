export enum statusClassText {
  BIG = 'text--big',
  MEDIUM = 'text--medium',
  SMALL = 'text--small--wh',
  SMALLEST = 'text--small--blk',
  DEFAULT = '',
}