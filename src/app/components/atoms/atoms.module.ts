import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AtomsRoutingModule } from './atoms-routing.module';
import { ButtonComponent } from './button/button.component';
import { TextComponent } from './text/text.component';
import { IconComponent } from './icon/icon.component';
import { BoxComponent } from './box/box.component';
import { TitleComponent } from './title/title.component';


@NgModule({
  declarations: [
    ButtonComponent,
    TextComponent,
    IconComponent,
    BoxComponent,
    TitleComponent,
  ],
  imports: [
    CommonModule,
    AtomsRoutingModule,
  ],
  exports :[
    ButtonComponent,
    TextComponent,
    IconComponent,
    TitleComponent,
    BoxComponent
  ]
})
export class AtomsModule { }
