import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { PokemonUser } from '../../molecules/interfaces/box-class.interface';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  @Output() emitPokemon = new EventEmitter<PokemonUser>();

  constructor() { }

  ngOnInit(): void {
  }

  sendPokemonSelected(pokemon: PokemonUser): void {
    this.emitPokemon.emit(pokemon);
  }

}
